<?php

$a = array("uno" => "1", "dos" => "2");
$b = array("tres" => "3", "cuatro" => "4", "cinco" => "5");

$c = $a + $b; // Unión de $a y $b
echo "Unión de \$a y \$b: <br>";
print_r($c); // Array ( [uno] => 1 [dos] => 2 [tres] => 3 [cuatro] => 4 [cinco] => 5 ) 

$myArray = array("manzana","platano","fresa");
echo (is_array($myArray)) ? "Is an array" : "Is not an array";

//is_int, is_float

?>