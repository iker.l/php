<style>body{font-family: monospace;}</style>
<?php

$altura = 6;
$anchura = 5;

for ($i=1; $i <= $altura; $i++) {
    if ($i == 1 || $i == $altura) {
        for ($j=1; $j <= $anchura; $j++) { 
            echo "*";
        }
        echo "<br>";
    } else {

        for ($j=0; $j < $anchura; $j++) { 
            // echo "j:$j";
            if ($j==0 || $j == $anchura-1) {
                echo "*";
            } else {
                echo "&nbsp;";
            }
        }
        echo "<br>";
    }
}


?>