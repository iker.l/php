<?php

// Dado un array cualquiera, por ejemplo
// Array = 1,2,56,78,23,78,23
// Buscar el número de apariciones de un número
// Num = 23
// EN este ejemplo el resultado es 2 (23 sale 2 veces en el array)
$myArray=array(1,2,56,78,23,78,23);
$numBuscado = 23;
$numApariciones = 0;
for($cont=0;$cont<count($myArray);$cont++) {
    if ($myArray[$cont] == $numBuscado ) {
        $numApariciones = $numApariciones + 1;
    }
}

echo "el $numBuscado aparece $numApariciones en array<br>";

?>