<?php
/**
 * Imprime SÓLO los números impares del 51 al 101 ambos incuidos
 * un númer por línea
 */

// user operador aritmetico modulo "%" para saber si un 
// numero es par o impar 


// 1º Aproximacion


for ($i=51;$i<=101;$i++){
        /*echo "$i";
        echo "<br>";
        */
    if ($i % 2 == 0) {
        // vacio
    } else {
        echo "$i <br>";
    }
}

// 2º aproximacion

for ($i=51;$i<=101;$i++){
    if ($i % 2 != 0){
        echo "$i <br>";
    }
}

?>


?>