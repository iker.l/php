<?php

/*
$i = 11;

do {
    echo $i;
} while ($i <= 10);
*/
//////////////////////////////////////////////////////
/*
$contador = 0;

while ($contador < 10) {
    if ($contador == 4) {
        echo "saliendo de bucle<br>";
        break;
    }
    $contador++;
}

echo "valor final de contador es:".$contador."<br>";
*/
//////////////////////////////////////////////////


$contador = 0;

while ($contador < 10) {
    if ($contador % 2 == 0) {
    	$contador++;
        echo "salto!<br>";
    	continue; # Salta a siguiente iteracion del bucle si es par
    }
    echo "Es impar: $contador<br>";
    $contador++;
}

?>